app = module.parent.exports.app;

//var router = express.Router();
var ShipmentController = require('../controllers/shipment');
var TrackingController = require('../controllers/tracking');
var ShippoController = require('../controllers/shippo');

/* GET home page. */



app.get('/', ShipmentController.index);
app.get('/shipment', ShipmentController.index);

app.get('/varma', function (req, res) {
    res.send('Here we are');
});


app.post('/addshipment/',ShipmentController.addShipmentId);

app.post('/updatecarrier/:shipmentId',ShipmentController.updateCarrier);

app.post('/updatestatus/:shipmentId',ShipmentController.updateStatus);

app.post('/sendsms/:shipmentId',ShipmentController.sendSms);

app.get('/tracking/:trackingId', ShipmentController.getTrackingDetailsByTrackingId);

app.get('/shipmentbydate/:startDate',ShipmentController.getShipmentDetailsByDate);

app.get('/shipment/:shipmentId', ShipmentController.getTrackingDetailsByShipmentId);

app.get('/shipment/:startDate/:endDate',ShipmentController.getShipmentDetailsBetweenDates);



app.get('/tracking/:provider/:trackingno', ShippoController.getTrackingDetailsByProvider);

