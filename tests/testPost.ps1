$JSON = @'
[ {               "Name" : "Domestic International",
                "FrieghtOrder" : 1,
                "Tracking" : [ 
                    {
                        "TrackingNo" : "121_dom",
                        "Status" : "SUCCESS",
                        "Status_Detail" : ".... What ever is the last"
                    }
                ],
                "Status" : "SUCCESS"}
            , 
            {
                "Name" : "FedEx",
                "FrieghtOrder" : 2,
                "Status" : "FAILURE",
                "Tracking" : [ 
                    {
                        "TrackingNo" : "779591314278",
                        "Status" : "TRANSIT",
                        "Status_Detail" : ".... What ever is the last"
                    }, 
                    {
                        "TrackingNo" : "779591314039",
                        "Status" : "FAILURE",
                        "Status_Detail" : "Delivery Exception"
                    }, 
                    {
                        "TrackingNo" : "779591314231",
                        "Status" : "SUCCESS",
                        "Status_Detail" : "Delivered"
                    }
                ]
            }
        ]
    

'@

$response = Invoke-WebRequest -Uri "http://localhost:3000/updatecarrier/1" -Method Post -Body $JSON -ContentType "application/json"