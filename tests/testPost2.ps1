$JSON = @'
{
    "ShipmentId" : 6,
    "CreatedDate" : "10/08/2017",
    "Status" : "success",
    "Sender" : {
        "Name" : "Padma",
        "City" : "Vizag",
        "Country" : "IN",
        "Branch" : "Vizag",
        "PhoneNo" : "+91 8790959634",
        "Email" : "eswar1251@gmail.com"
    },
    "Receiver" : {
        "Name" : "Goutham",
        "City" : "Saudhi Setu Garu",
        "Country" : "Saudhi",
        "PhoneNo" : "+1 9985099212",
        "Email" : "t.goutham007@mail.com"
    },
    "TrackingHistory" : [ 
        {
            "TrackingNo" : "2UPS",
            "Name" : "Domestic",
            "Status_Details" : "Completed",
            "Status_Date" : "date with time",
            "Display_Order" : 1,
            "Location" : {
                "City" : "Hyderabad",
                "State" : "TS",
                "ZIP" : "530021",
                "Country" : "IN"
            }
        }
    ],
    "Carriers" : [ 
        {
            "Name" : "Domestic International",
            "FrieghtOrder" : 1,
            "Tracking" : [ 
                {
                    "TrackingNo" : "121_dom",
                    "Status" : "SUCCESS",
                    "Status_Detail" : ".... What ever is the last"
                }
            ],
            "Status" : "SUCCESS"
        }, 
        {
            "Name" : "FedEx",
            "FrieghtOrder" : 2,
            "Status" : "FAILURE",
            "Tracking" : [ 
                {
                    "TrackingNo" : "779591314278",
                    "Status" : "TRANSIT",
                    "Status_Detail" : ".... What ever is the last"
                }, 
                {
                    "TrackingNo" : "779591314039",
                    "Status" : "FAILURE",
                    "Status_Detail" : "Delivery Exception"
                }, 
                {
                    "TrackingNo" : "779591314231",
                    "Status" : "SUCCESS",
                    "Status_Detail" : "Delivered"
                }
            ]
        }
    ]
}
'@

$response = Invoke-WebRequest -Uri "http://localhost:3000/addshipment/" -Method Post -Body $JSON -ContentType "application/json"