var util = require('util');


var DatabaseConfig = {
    poolSize: Number,
    grvname: String,
    grvuser: String,
    grvpass: String,
    grvhost: String,
    grvport: Number,
    
    urigrvdb: String, 
    urigrvdb_write: String, 
   
};




function isCluster(host){
    var regExp = new RegExp(":", "gi");
    return ((host.match(regExp) || []).length > 1) ;
}

function getConnectionURI (host, port, dbname, replset, user, password, isForRead) {
    
    var retURI = "";

    var isClusterConfig = isCluster(host);

   // console.log("IS Cluster - " + host);

    // if it is not a cluster and if you got host and port separately then add it
    if (!isClusterConfig) {

        host = host + ":" + port;
    }


    if (user == '') {
        retURI = 'mongodb://' + host + '/' + dbname;
    }
    else {
        retURI = 'mongodb://' + user + ':' + password + '@' + host + '/' + dbname;

    }

    // if it is a cluster and if it is for write
    if (( isClusterConfig == true) && (isForRead == true) ) {

        retURI += '?w=1&readPreference=primary';

    } // if it is a  cluster and for read only
    else if (( isClusterConfig == true) && (isForRead == false)) {

        console.log("read - only cluster");

        retURI += '?readPreference=secondaryPreferred';

    }

    if (( isClusterConfig == true)  && replset != "" && (replset != undefined)) {

        retURI += '&replicaSet=' + replset;

    }

    return retURI;

}

function setConfig() {

    //console.log ("In common setConfig");
    //EnvConfig.environment = setEnv("DATACITY_ENV", process.env.DATACITY_ENV);

    var mongoAddressToUse =  "ds153732.mlab.com";
    var grvNameToUse = "gv_test_db";
    var grvUserToUse =  "gvtestuser";
    var grvPassToUse =  "_9YfEFfUQh6gRuk";
    var grvhost =  mongoAddressToUse;
    var nodePortToUse =  3000;


    
    DatabaseConfig.grvname = grvNameToUse;
    DatabaseConfig.grvport = 13106;
    DatabaseConfig.grvhost = grvhost;
    DatabaseConfig.grvuser = grvUserToUse;
    DatabaseConfig.grvpass = grvPassToUse;

    /*EnvConfig.port = process.env.PORT || 3001;
    EnvConfig.grvURL = setEnv("grv_URL", process.env.grv_URL);
    EnvConfig.baseIndexPath = setEnv("Location of the base path - BASE_IDX_PATH", process.env.BASE_IDX_PATH ) ; //|| '/home/donwb/projects/bondo';
    EnvConfig.rawIndexPath =  EnvConfig.baseIndexPath + "/public/index.html";
*/
    
    
    if(mongoAddressToUse) {
        mongoAddressToUse = mongoAddressToUse.replace(/mongodb:\/\//g,'');
    }

    // DB Configurations Env Variables(for MongoLab)

    var grvHost =  mongoAddressToUse ;
    var grvPort =  53732;


 

    // grv
    DatabaseConfig.grvname = grvNameToUse;
    DatabaseConfig.grvuser = grvUserToUse;
    DatabaseConfig.grvpass = grvPassToUse;
    DatabaseConfig.grvhost = grvHost;
    DatabaseConfig.grvport = grvPort;


    DatabaseConfig.urigrvdb_write = getConnectionURI(
        DatabaseConfig.grvhost,
        DatabaseConfig.grvport,
        DatabaseConfig.grvname,
        DatabaseConfig.grv_replicaSet,
        DatabaseConfig.grvuser,
        DatabaseConfig.grvpass,
        false);

    DatabaseConfig.urigrvdb = getConnectionURI(
        DatabaseConfig.grvhost,
        DatabaseConfig.grvport,
        DatabaseConfig.grvname,
        DatabaseConfig.grv_replicaSet,
        DatabaseConfig.grvuser,
        DatabaseConfig.grvpass,
        true); // is for Read only

       


   // console.log("DatabaseConfig ------");
    console.log(util.inspect(DatabaseConfig, {showHidden: false, depth: null}));
    //console.log("EnvConfig ------");
    //console.log(util.inspect(EnvConfig, {showHidden: false, depth: null}));
}






DatabaseConfig.poolSize = 10;
module.exports.DatabaseConfig = DatabaseConfig;
//module.exports.EnvConfig = EnvConfig;

module.exports.setConfig = setConfig;

