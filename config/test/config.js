var util = require('util');

var ElasticConfig = {
    env : String,
    port: Number,
    host: String,
    limit: Number,
    isElasticCloudConfig : Number, // IS_ELASTIC_CLOUD configuration - requires user id and password
    cluserid: String,
    username: String,
    password: String,
    promo_es_env: String,
    promo_port: Number,
    promo_host: String,
    promo_limit: Number,
    promo_cluserid: String,
    promo_username: String,
    promo_password: String,
    esConnParm: {},
    esPromoConnParm: {}
};

var RedisConfig = {
    port: Number,
    host: String,
    useredis: Boolean,
    TTL: Number,
    APITTL: Number
};

var DatabaseConfig = {
    env :String,
    poolSize: Number,
    bondoservers: [],
    froggerservers: [],

    bondoname: String,
    bondouser: String,
    bondopass: String,

    flowname: String,
    flowuser: String,
    flowpass: String,
    flowhost: String,
    flowport: Number,

    flowname2: String,
    flowuser2: String,
    flowpass2: String,
    flowhost2: String,
    flowport2: Number,

    froggername: String,
    froggeruser: String,
    froggerpass: String,
    froggerhost: String,
    froggerport: Number,

    // these are for the read operations
    uriflowdb: String, 
    uriflowdb2: String,
    uriflowdb_write: String, 
    uriflowdb2_write: String,

    flow_replicaSet: String,
    flow2_replicaSet: String,
    bondo_replicaSet: String,
    frogger_replicaSet: String,

    flow_dns: String,
    flow_api_key: String,
    flow_method: String,
    
    isInDebug : Number, // ISDEBUG environment variable
    isConsoleLog : Number, // ISCONSOLELOG environment variable
    logz_token  : String // logz token - valid if defined.

};

var EnvConfig = {
    port: Number,
    dirname: String,
    address: String,
    acceptableSubnets: String,
    HTTPSFLOW : String // to flush https//*data-city if someone calls http
};

var S3Config = {

    s3_env : String,
    accesskey: String,
    secretAccessKey : String,
    bucket : String
};

/*
    host - host name
    port - db port
    dbname - name of the database
    replSet - replicaSet - if any
    user - user name
    password - password
    isForRed - true or flase will set the read or write preference

 */

function isCluster(host){
    var regExp = new RegExp(":", "gi");
    return ((host.match(regExp) || []).length > 1) ;
}

function getESIndexName(regName) {

    return ElasticConfig.es_env + ".dc." + regName;

}

function getESPromosIndexName(regName) {

    return ElasticConfig.promo_es_env + ".promos." + regName;

}

function getConnectionURI (host, port, dbname, replset, user, password, isForRead) {

    var retURI = "";

    var hostPort = host;
    var isClusterConfig = isCluster(host);

    console.log("IS Cluster - " + isCluster(host));

    // if it is not a cluster and if you got host and port separately then add it
    if (!isClusterConfig) {

        host = host + ":" + port;
    }


    if (user == '') {
        retURI = 'mongodb://' + host + '/' + dbname;
    }
    else {
        retURI = 'mongodb://' + user + ':' + password + '@' + host + '/' + dbname;

    }

    // if it is a cluster and if it is for write
    if (( isClusterConfig == true) && (isForRead == true) ) {

        retURI += '?w=1&readPreference=primary';

    } // if it is a  cluster and for read only
    else if (( isClusterConfig == true) && (isForRead == false)) {

        console.log("read - only cluster");

        retURI += '?readPreference=secondaryPreferred';

    }

    if (( isClusterConfig == true)  && replset != "" && (replset != undefined)) {

        retURI += '&replicaSet=' + replset;

    }

    return retURI;

}

function setEnv(envStr, envValue) {

    if (envValue === undefined) {
        console.error("Environment not completely defined!! - execute environment config file!!");
        console.error("Parameter - " + envStr + " is missing!");
        process.exit(1);
    }
    return envValue;
}

function setESConfig() {

    var host = {};
    var promohost = {};

    ElasticConfig.env = setEnv("DATACITY_ENV", process.env.DATACITY_ENV);
    ElasticConfig.promo_es_env = process.env.ES_PROMO_ELASTIC_ENV || ElasticConfig.env;

    // use this as environment variable if defined. This will help work in other environments easily.
    ElasticConfig.es_env = process.env.ES_DATACITY_ELASTIC_ENV || process.env.DATACITY_ENV;


    var isCloud = process.env.IS_ELASTIC_CLOUD;
    ElasticConfig.isElasticCloudConfig = isCloud || "0";

    // If this is Elastic Cloud Configuration then make these required params
    if ( ElasticConfig.isElasticCloudConfig === "1") {
        if (ElasticConfig.promo_es_env !== "") {
            ElasticConfig.promo_username = process.env.ES_PROMO_USER || process.env.ES_USER;
            ElasticConfig.promo_password = process.env.ES_PROMO_PWD || process.env.ES_PWD;
            ElasticConfig.promo_cluserid = process.env.ES_PROMO_CLUSTER || process.env.ES_CLUSTER;

            var elasticPromoAddressToUse = process.env.ES_PROMO_SERVER || process.env.ES_SERVER;
            var elasticPromoPortToUse = process.env.ES_PROMO_PORT || process.env.ES_PORT;
            var elasticPromoLimitToUse = process.env.ES_PROMO_LIMIT || 1000; // 1000 records

            ElasticConfig.promo_host = elasticPromoAddressToUse;
            ElasticConfig.promo_port = elasticPromoPortToUse;
            ElasticConfig.promo_limit = elasticPromoLimitToUse;

            promohost.port = elasticPromoPortToUse;
            promohost.host = elasticPromoAddressToUse;

            promohost.protocol = "https";
            promohost.auth = ElasticConfig.promo_username + ":" + ElasticConfig.promo_password;

            console.log("Promo Elastic Cloud Hosting Configuration Set!");
        }
        if (ElasticConfig.env !== "") {
            ElasticConfig.username = setEnv("ES_USER", process.env.ES_USER);
            ElasticConfig.password = setEnv("ES_PWD", process.env.ES_PWD);
            ElasticConfig.cluserid = setEnv("ES_CLUSTER", process.env.ES_CLUSTER);

            var elasticAddressToUse = setEnv("ES_SERVER", process.env.ES_SERVER);
            var elasticPortToUse = setEnv("ES_PORT", process.env.ES_PORT);
            var elasticLimitToUse = process.env.ES_LIMIT || 1000; // 1000 records

            ElasticConfig.host = elasticAddressToUse;
            ElasticConfig.port = elasticPortToUse;
            ElasticConfig.limit = elasticLimitToUse;

            host.port = elasticPortToUse;
            host.host = elasticAddressToUse;

            host.protocol = "https";
            host.auth = ElasticConfig.username + ":" + ElasticConfig.password;

            console.log("Elastic Cloud Hosting Configuration Set!");
        }
    }
    else {

        var elasticAddressToUse = setEnv("ES_SERVER", process.env.ES_SERVER ) ;
        var elasticPortToUse = setEnv("ES_PORT", process.env.ES_PORT) ;
        var elasticLimitToUse = process.env.ES_LIMIT || 1000; // 500 records

        ElasticConfig.host = elasticAddressToUse;
        ElasticConfig.port = elasticPortToUse;
        ElasticConfig.limit = elasticLimitToUse;

        host.port = elasticPortToUse;
        host.host = elasticAddressToUse;

        host.protocol = "http";
        console.log("Elastic Local Hosting Configuration Set!");
    }

    ElasticConfig.esConnParm.host = host;
    ElasticConfig.esConnParm.requestTimeout = 1000*60;
    ElasticConfig.esConnParm.maxSockets = 20;
    ElasticConfig.esConnParm.maxRetries = 5;
    ElasticConfig.promo_es_env = process.env.ES_PROMO_ELASTIC_ENV || ElasticConfig.env;

    ElasticConfig.esPromoConnParm.host = promohost;
    ElasticConfig.esPromoConnParm.requestTimeout = 1000*60;
    ElasticConfig.esPromoConnParm.maxSockets = 20;
    ElasticConfig.esPromoConnParm.maxRetries = 5;

    console.log(util.inspect(ElasticConfig, {showHidden: false, depth: null}));

}

function setArgoConfig() {

    DatabaseConfig.env = setEnv("DATACITY_ENV", process.env.DATACITY_ENV);
    DatabaseConfig.flow_dns = setEnv("FLOW_DNS", process.env.FLOW_DNS);
    DatabaseConfig.flow_api_key = setEnv("FLOW_API_KEY", process.env.FLOW_API_KEY);
    DatabaseConfig.flow_method = setEnv("FLOW_METHOD", process.env.FLOW_METHOD);
    

    var mongoAddressToUse = process.env.MONGODB || "srd-10-60-177-120.nodes.56m.dmtio.net";
    var elasticAddressToUse = process.env.ES_PROXY_URL || "localhost";
    var elasticPortToUse = process.env.ES_PORT || 9200;
    var elasticLimitToUse = process.env.ES_LIMIT || 1000; // 500 records
    var redisAddressToUse = process.env.REDIS_URL || "localhost";
    var redisPortToUse = process.env.REDIS_PORT || 6379;
    var bondoNameToUse = process.env.BONDONAME || "bondo";
    var bondoUserToUse = process.env.BONDOUSER || "bondo_reader";
    var bondoPassToUse = process.env.BONDOPASS || "readerbondo";
    var flowNameToUse = process.env.FLOWNAME || "flow";
    var flowUserToUse = process.env.FLOWUSER || "flow_user";
    var flowPassToUse = process.env.FLOWPASS || "userfl0w!";
    var flow2NameToUse = process.env.FLOW2NAME || "flow2";
    var flow2UserToUse = process.env.FLOW2USER || "flow_user";
    var flow2PassToUse = process.env.FLOW2PASS || "userfl0w!";
    var froggerNameToUse = process.env.FROGGERNAME || "frogger";
    var froggerUserToUse = process.env.FROGGERUSER || "frogger_writer";
    var froggerPassToUse = process.env.FROGGERPASS || "writerfrogger";
    var nodePortToUse = process.env.NODE_PORT || 3000;


    console.log("Redis server --> " + redisAddressToUse);
    console.log("Redis port --> " + redisPortToUse);
    
    // if logz token is defined then use it
    DatabaseConfig.logz_token = process.env.LOGZ_TOKEN || "";

    console.log("Logz Token --> " + DatabaseConfig.logz_token);


    // Replica Sets
    DatabaseConfig.flow_replicaSet = process.env.FLOW_REPL || "";
    DatabaseConfig.flow2_replicaSet = process.env.FLOW2_REPL || "";
    DatabaseConfig.bondo_replicaSet = process.env.BONDO_REPL || "";
    DatabaseConfig.frogger_replicaSet = process.env.BONDO_REPL || "";

    // Debug configuration
    DatabaseConfig.isInDebug = process.env.ISDEBUG||0;

    // Logging Configuration
    DatabaseConfig.isConsoleLog = process.env.ISCONSOLELOG || 1;

    if(mongoAddressToUse) {
      mongoAddressToUse = mongoAddressToUse.replace(/mongodb:\/\//g,'');
    }

    // DB Configurations Env Variables(for MongoLab) 
    var bondoHost = process.env.BONDO_HOST || mongoAddressToUse ;
    var bondoPort = process.env.BONDO_PORT || 27017;

    var flowHost = process.env.FLOW_HOST || mongoAddressToUse ;
    var flowPort = process.env.FLOW_PORT || 27017;

    var flow2Host = process.env.FLOW2_HOST || mongoAddressToUse ;
    var flow2Port = process.env.FLOW2_PORT || 27017;

    var froggerHost = process.env.FROGGER_HOST || mongoAddressToUse ;
    var froggerPort = process.env.FROGGER_PORT || 27017;

    var redisTTL = process.env.REDIS_TTL || 900;
    var redisAPITTL = process.env.REDIS_API_TTL || 86400;

    // bondo
    DatabaseConfig.bondoname = bondoNameToUse;
    DatabaseConfig.bondouser = bondoUserToUse;
    DatabaseConfig.bondopass = bondoPassToUse;
    DatabaseConfig.bondohost = bondoHost;
    DatabaseConfig.bondoport = bondoPort;

    DatabaseConfig.bondoservers.push({
        endpoint: bondoHost,
        port: bondoPort
    });

    // flow
    DatabaseConfig.flowname = flowNameToUse;
    DatabaseConfig.flowuser = flowUserToUse;
    DatabaseConfig.flowpass = flowPassToUse;
    DatabaseConfig.flowhost = flowHost;
    DatabaseConfig.flowport = flowPort;

    DatabaseConfig.flowname2 = flow2NameToUse;
    DatabaseConfig.flowuser2 = flow2UserToUse;
    DatabaseConfig.flowpass2 = flow2PassToUse;
    DatabaseConfig.flowhost2 = flow2Host;
    DatabaseConfig.flowport2 = flow2Port;

    // frogger
    DatabaseConfig.froggername = froggerNameToUse;
    DatabaseConfig.froggeruser = froggerUserToUse;
    DatabaseConfig.froggerpass = froggerPassToUse;
    DatabaseConfig.froggerhost = froggerHost;
    DatabaseConfig.froggerport = froggerPort;



    // Redis

    // when in the format of tcp://127.0.0.1:6379/
    if (redisAddressToUse.toString().replace('tcp://', '').indexOf(':') > -1) {        
        var arr = redisAddressToUse.toString().replace('tcp://', '').split(':');
        RedisConfig.host = arr[0];
        RedisConfig.port = arr[1].replace('/', '');
    }
    else {
        RedisConfig.host = redisAddressToUse;
        RedisConfig.port = redisPortToUse; 
    }

    RedisConfig.useredis = true;
    RedisConfig.TTL = redisTTL ; // time to keep items in cache in seconds
    RedisConfig.APITTL = redisAPITTL ; // time to keep items in cache in seconds

    EnvConfig.port = nodePortToUse;
    EnvConfig.address = process.env.FLOW_DNS ;
    EnvConfig.HTTPSFLOW = process.env.HTTPSFLOW ; // if this is defined, then flush happen parallely


    DatabaseConfig.uriflowdb_write = getConnectionURI(
                    DatabaseConfig.flowhost,
                    DatabaseConfig.flowport,
                    DatabaseConfig.flowname,
                    DatabaseConfig.flow_replicaSet,
                    DatabaseConfig.flowuser,
                    DatabaseConfig.flowpass,
                    false);

    DatabaseConfig.uriflowdb = getConnectionURI(
                    DatabaseConfig.flowhost,
                    DatabaseConfig.flowport,
                    DatabaseConfig.flowname,
                    DatabaseConfig.flow_replicaSet,
                    DatabaseConfig.flowuser,
                    DatabaseConfig.flowpass,
                    true); // is for Read only

    DatabaseConfig.uriflowdb2_write = getConnectionURI(
                    DatabaseConfig.flowhost2,
                    DatabaseConfig.flowport2,
                    DatabaseConfig.flowname2,
                    DatabaseConfig.flow2_replicaSet,
                    DatabaseConfig.flowuser2,
                    DatabaseConfig.flowpass2,
                    false);

    DatabaseConfig.uriflowdb2 = getConnectionURI(
                    DatabaseConfig.flowhost2,
                    DatabaseConfig.flowport2,
                    DatabaseConfig.flowname2,
                    DatabaseConfig.flow2_replicaSet,
                    DatabaseConfig.flowuser2,
                    DatabaseConfig.flowpass2,
                    true); // is for Read only

    DatabaseConfig.uribondo  = getConnectionURI(
                    DatabaseConfig.bondohost,
                    DatabaseConfig.bondoport,
                    DatabaseConfig.bondoname,
                    DatabaseConfig.bondo_replicaSet,
                    DatabaseConfig.bondouser,
                    DatabaseConfig.bondopass,
                    false); // is for Read only

    console.log ("flow connection string ->" + DatabaseConfig.uriflowdb);
    console.log ("flow2 connection string ->" + DatabaseConfig.uriflowdb2);

    console.log(util.inspect(DatabaseConfig, {showHidden: false, depth: null}));

    console.log("------------------------------------------------------------");


    setESConfig();

    setS3config();
    console.log("------------------------------------------------------------");



}


function getS3Path(key) {

    if (key.startsWith("dc/" + S3Config.s3_env  + "/"))  {
        return key;
    } else {
        return "dc/" + S3Config.s3_env + "/" + key;
    }
}

function setS3config() {

    S3Config.accesskey = setEnv("S3_ACCESS_KEY", process.env.S3_ACCESS_KEY);
    S3Config.secretAccessKey = setEnv("S3_SECRET_KEY", process.env.S3_SECRET_KEY);
    S3Config.bucket = setEnv("S3_BUCKET", process.env.S3_BUCKET);

    // if S3_DATACITY_ENV does not exist then take it from DATACITY_ENV
    S3Config.s3_env = process.env.S3_DATACITY_ENV || process.env.DATACITY_ENV ;

}

DatabaseConfig.poolSize = 10;

module.exports.RedisConfig = RedisConfig;
module.exports.ElasticConfig = ElasticConfig;
module.exports.DatabaseConfig = DatabaseConfig;
module.exports.EnvConfig = EnvConfig;
module.exports.getESIndexName = getESIndexName;
module.exports.getESPromosIndexName = getESPromosIndexName;

module.exports.getS3Path = getS3Path;
module.exports.S3Config = S3Config;

module.exports.setArgoConfig = setArgoConfig;
