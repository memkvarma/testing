config = require('../config/config');
mongodb = require('mongodb');
var settings = config.DatabaseConfig;

console.log("In Grv Provider");


console.log('module: ' + module.id + ' has loaded...');

var grvdb = initializeConnect();

grvdb.on('open', function() {
    console.log((new Date()).toString() + ' grvdb open');
});

grvdb.on('close', function() {
    console.log((new Date()).toString() + ' grvdb close');
});

grvdb.on('error', function() {
    console.log((new Date()).toString() + ' grvdb error');
});

grvdb.on('connecting', function() {
    console.log((new Date()).toString() + ' grvdb connecting');
});

grvdb.on('connected', function() {
    console.log((new Date()).toString() + ' grvdb connected');
});

grvdb.on('reconnected', function() {
    console.log((new Date()).toString() + ' grvdb reconnected');
})

grvdb.on('disconnected', function() {
    console.log((new Date()).toString() + ' grvdb disconnected');
});

GrvProvider = function() {};

GrvProvider.prototype.resetConnection = function(req, res) {
    console.log("grvdb Connection Reset");
    counter = 0;
    try {
        initializeConnect();
    } catch (e) {
        console.error("Error Handler");
        console.log(e);
    }

    return;
};

function initializeConnect() {


    console.log(config);
    console.log('Connecting to flow ' + settings.urigrvdb_write);

    return mongodb.MongoClient.connect(settings.urigrvdb_write, {
        server: {
            auto_reconnect: true,
            socketOptions: {
                keepAlive: 1
            },
            poolSize: 10
        },useMongoClient: true,
    });
}

module.exports.grvdb = grvdb;
exports.GrvProvider = GrvProvider;

