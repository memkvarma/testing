var shipmentModelProvider= require('../models/shipmentModel').shipmentModelProvider;
var shipmentModelProvider = new shipmentModelProvider();

var http = require('http');    
var https = require('https');

//config = require('../config/config');

var React = require('react');


function StringtoDate(txt) {

    var count=0;
    var result="";
    num=2;
    sep='/';
    for (var i = 0; i < txt.length; i += num) {
        if(count>=2){ num=4; sep=""; }    
        result += txt.substr(i, num)+sep;
        count++;
    }
    var string=result;
    return string;
}


exports.addShipmentId = function(req, res, callback) {

            var data= req.body;
            shipmentModelProvider.insertData(data, function(doc) {
            res.sendStatus('200');
            }); 
};

exports.index=function(req,res,callback){
    
     shipmentModelProvider.findAll(null,function(doc) {
           //console.log(doc);
            //res.json(doc);
            data=JSON.stringify(doc);
            //res.render('App', { name: "varma",list: data});
            res.render('index.jsx', { name: 'world',list: data });
    }); 
        
 
};

exports.updateCarrier = function(req, res, callback) {
        shipmentModelProvider.updateCarrier(req, function(doc) {
            res.sendStatus('200');
    }); 
};

exports.updateStatus=function(req,res,callback){
    res.sendStatus('200');
};

exports.getTrackingDetailsByTrackingId=function(req,res,callback){
    console.log(req.params.trackingId);
        var id=req.params.trackingId;
        shipmentModelProvider.findByTrackingId(id, function(doc) {
            //  console.log(doc);
            res.json(doc);
    }); 
};

exports.getShipmentDetailsBetweenDates=function(req,res,callback){
    
   var startDate=req.params.startDate;
   var endDate=req.params.endDate; 
        startDate = StringtoDate(startDate);
        endDate = StringtoDate(endDate);
    var arr={'startDate':startDate,'endDate':endDate};
     shipmentModelProvider.findByDates(arr, function(doc) {
            //  console.log(doc);
            res.json(doc);
    });

   
};

exports.getShipmentDetailsByDate=function(req,res,callback){
    
   var  startDate=req.params.startDate;
        startDate = StringtoDate(startDate);
        console.log(startDate);
        shipmentModelProvider.findByDate(startDate, function(doc) {
                //  console.log(doc);
                res.json(doc);
        });

   
};

exports.sendSms=function(req,res,callback){
        res.sendStatus('200');
};
    
exports.getTrackingDetailsByShipmentId = function(req, res) {       
        console.log(req.params.shipmentId);
        var id=parseInt(req.params.shipmentId);
        shipmentModelProvider.findById(id, function(doc) {
            //  console.log(doc);
            res.json(doc);
    }); 
    
};