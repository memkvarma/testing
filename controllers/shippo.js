var http = require('http');    
var https = require('https');
// replace <YOUR_PRIVATE_KEY> with your ShippoToken key
var shippo = require('shippo')('shippo_test_0b7108ca508f12dae640da2fdd4378f3931caf05');

exports.getTrackingDetailsByProvider = function(req, res) {
// example Tracking object for tracking a shipment
var provider=req.params.provider;
console.log(provider);
var trackingno=req.params.trackingno;
shippo.track.get_status(provider, trackingno)
.then(function(status) {
	//status=JSON.stringify(status);
    res.send(status);
	//	console.log("Tracking info: %s", JSON.stringify(status, null, 4));
}, function(err) {
     res.send('There was an error retrieving tracking information:');
	//	console.log("There was an error retrieving tracking information: %s", err);
});
};

