var express = require('express');
var path = require('path');
var logger = require('morgan');
var compression = require('compression');
var errorhandler = require('errorhandler');
var bodyParser = require('body-parser');
var app = module.exports = express();
var http = require('http');
var config = require('./config/config');
var debug = require('debug')('grv:server');

var React = require('react');
var ReactDOMServer = require('react-dom/server');
var ReactDom=require('react-dom');

var ReactEngine = require('express-react-engine');

require('node-jsx').install();const cors = require('cors');
const corsOptions = {origin: 'http://localhost:3001'};


const { createEngine } = require('express-react-views');

config.setConfig();

//var cookieParser = require('cookie-parser');
//var favicon = require('serve-favicon');

/**
 * Get port from environment and store in Express.
 */

app.rootdirectory = __dirname;
app.use(compression());
app.use(bodyParser.json({limit: '10mb'}));

app.use(cors(corsOptions)); //http status error
app.disable('etag'); //for disable 304


// the view files are JavaScript files, hence the extension

// the directory containing the view files
app.set('views', './views');

app.set('view engine', 'jsx');
app.engine('jsx', createEngine());
//app.engine('jsx', ReactEngine());

var port = normalizePort(process.env.PORT || '7000');
app.set('port', port);

app.use(express.static(__dirname + '/public'));
app.use(errorhandler({
    dumpExceptions: true,
    showStack: true
}));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/*var MongoClient = require('mongodb').MongoClient;
MongoClient.connect('mongodb://gvtestuser:_9YfEFfUQh6gRuk@ds153732.mlab.com:53732/gv_test_db', function(err, db) {
    if (err) {
        console.error(err);
    }
    var collection = db.collection('shipments');
    collection.find().toArray(function(err, docs) {
        console.log(docs);
    });
});*/




/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}





// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', routes);
//app.use('/users', users);


module.exports.app = app;



var routes = require('./routes/route');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
    res.json({ error: err });
});







