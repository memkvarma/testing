var mongo = require('mongodb');
var db = mongo.Db;
var config = require('../config/config');
var MongoClient = require('mongodb').MongoClient;
var settings = config.DatabaseConfig;
var util = require('util');
var assert = require('assert');

function connect(cb) {
    MongoClient.connect(settings.urigrvdb, function(err, db) {

        if (err) {
            cb(err);
        }
        else {
            db.authenticate(settings.grvuser, settings.grvpass, function(err, res) {
                if(!err) {
                    cb(null, db);
                } else {
                    console.log((new Date()).toString() + " Error in authentication.");
                    console.log((new Date()).toString() + err);
                    cb(err);
                }
            });
        }
    });
}

connect(function(err, theDb){
    if (err) {
        console.log((new Date()).toString() + err);
        process.exit(1);
    }

    db = theDb;
});

shipmentModelProvider = function () { };

    var defintionCollection = 'shipments';

shipmentModelProvider.prototype.insertData = function(req, callback) {
      
     var data=req;
     console.log(data);
     var collectionName = defintionCollection;
     var collection = db.collection(collectionName);
     collection.insert(data, function(err) {
        if(err) throw err;  
        callback(200);   
    });
};

shipmentModelProvider.prototype.findById = function(req, callback) {

    var collection = db.collection(defintionCollection);
    var id = parseInt(req);
    var query = {"ShipmentId":id};

     collection.find(query).toArray(function(err, items) {
            if(err) throw err;  
            callback(items)         
        });

};

shipmentModelProvider.prototype.findByTrackingId=function(req, callback) {

    var collection = db.collection(defintionCollection);
    var id = req;
    var query = {'TrackingHistory.TrackingNo':id};

     collection.find(query).toArray(function(err, items) {
            if(err) throw err;  
            callback(items)         
        });

};

shipmentModelProvider.prototype.findAll = function(req, callback) {
 	
   
    var collection = db.collection(defintionCollection);
    var query = {};
    collection.find(query).toArray(function(err, items) {
            if(err) throw err;  
            callback(items)         
        });
};

shipmentModelProvider.prototype.findByDates= function(req, callback) {
    var startDate = req.startDate;
    var endDate = req.endDate;
    var collectionName = defintionCollection;
    var collection = db.collection(collectionName);
    var query = {'CreatedDate':{$gte:startDate,$lte:endDate}};
    console.log(query);
    collection.find(query).toArray(function(err, items) {
            if(err) throw err;  
            callback(items)         
        });
};
    
shipmentModelProvider.prototype.findByDate= function(req, callback) {
    var startDate = req;
    var collectionName = defintionCollection;
    var collection = db.collection(collectionName);
    var query = {'CreatedDate':startDate};
    console.log(query);
    collection.find(query).toArray(function(err, items) {
            if(err) throw err;  
            callback(items)         
        });
};


shipmentModelProvider.prototype.updateCarrier= function(req, callback) {
    var id = parseInt(req.params.shipmentId);
    var carrier=req.body;
    var collectionName = defintionCollection;
    var collection = db.collection(collectionName);
    var bool=true;
    
    collection.update({'ShipmentId': id},{ $set: { 'Carriers':carrier }},{ upsert: true } , function(err, result) {
            if(err) throw err;  
            callback(result);         
        });
    
};

exports.shipmentModelProvider = shipmentModelProvider;
